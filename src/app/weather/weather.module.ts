import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WeatherComponent} from './components/weather/weather.component';
import {FormsModule} from '@angular/forms';
import {CityFormComponent} from './components/city-form/city-form.component';

@NgModule({
    declarations: [WeatherComponent, CityFormComponent],
    imports: [CommonModule, FormsModule],
})
export class WeatherModule {}
