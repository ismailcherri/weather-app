import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {loadWeathersByCoordinates} from 'src/app/store/actions/weather.actions';
import {selectWeatherStateData} from 'src/app/store/selectors/weather.selectors';

@Component({
    selector: 'app-weather',
    templateUrl: './weather.component.html',
    styleUrls: ['./weather.component.scss'],
})
export class WeatherComponent implements OnInit {
    state$ = this.store.select(selectWeatherStateData);

    constructor(private store: Store) {}

    ngOnInit(): void {
        this.store.dispatch(loadWeathersByCoordinates({request: {units: 'metric', lat: Math.random() * 89, lon: Math.random() * 179}}));
    }
}
