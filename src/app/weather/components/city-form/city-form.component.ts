import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {combineLatest} from 'rxjs';
import {selectWeatherStateData, selectWeatherStateError, selectWeatherStatePending} from 'src/app/store/selectors/weather.selectors';
import {map} from 'rxjs/operators';
import {NgForm} from '@angular/forms';
import {loadWeathersByCity} from 'src/app/store/actions/weather.actions';

@Component({
    selector: 'app-city-form',
    templateUrl: './city-form.component.html',
    styleUrls: ['./city-form.component.scss'],
})
export class CityFormComponent {
    state$ = combineLatest([this.store.select(selectWeatherStatePending), this.store.select(selectWeatherStateError)]).pipe(
        map(([pending, error]) => ({pending, error})),
    );

    constructor(private store: Store) {}

    updateWeatherDate(form: NgForm): void {
        this.store.dispatch(loadWeathersByCity({request: {units: 'metric', city: form.controls.city.value}}));
    }
}
