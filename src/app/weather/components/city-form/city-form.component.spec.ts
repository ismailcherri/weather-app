import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CityFormComponent} from './city-form.component';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {initialState} from 'src/app/store/reducers/weather.reducer';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';

describe('CityFormComponent', () => {
    let component: CityFormComponent;
    let fixture: ComponentFixture<CityFormComponent>;
    let store: MockStore;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [CityFormComponent],
            providers: [provideMockStore({initialState}), RouterTestingModule],
            imports: [FormsModule],
        }).compileComponents();

        store = TestBed.inject(MockStore);
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CityFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
