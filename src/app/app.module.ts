import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {StoreModule} from '@ngrx/store';
import * as fromWeather from './store/reducers/weather.reducer';
import {EffectsModule} from '@ngrx/effects';
import {WeatherEffects} from './store/effects/weather.effects';
import {AppComponent} from './app.component';
import {SharedModule} from 'src/app/shared/shared.module';
import {RouterModule} from '@angular/router';
import {WeatherComponent} from 'src/app/weather/components/weather/weather.component';
import {WeatherModule} from 'src/app/weather/weather.module';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        FormsModule,
        SharedModule,
        WeatherModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
        StoreModule.forFeature(fromWeather.weatherFeatureKey, fromWeather.reducer),
        EffectsModule.forFeature([WeatherEffects]),
        RouterModule.forRoot([{path: '**', component: WeatherComponent}]),
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
