import {NgModule} from '@angular/core';
import {WeatherService} from 'src/app/shared/service/weather.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {CacheInterceptor} from 'src/app/shared/interceptor/cache-interceptor';

@NgModule({
    declarations: [],
    imports: [HttpClientModule],
    providers: [WeatherService, {provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true}],
})
export class SharedModule {}
