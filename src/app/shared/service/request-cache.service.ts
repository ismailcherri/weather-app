import {Injectable} from '@angular/core';
import {HttpRequest, HttpResponse} from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})
export class RequestCacheService {
    private cache: {[url: string]: unknown} = {};

    public getCacheKey(request: HttpRequest<unknown>): string {
        return request.url + request.params.toString();
    }

    public get(request: HttpRequest<unknown>): any {
        return this.cache[this.getCacheKey(request)];
    }

    public put(request: HttpRequest<unknown>, response: HttpResponse<unknown>): void {
        this.cache[this.getCacheKey(request)] = response;
    }
}
