import {TestBed} from '@angular/core/testing';

import {RequestCacheService} from './request-cache.service';
import {HttpParams, HttpRequest} from '@angular/common/http';

describe('RequestCacheService', () => {
    let service: RequestCacheService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(RequestCacheService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should generate cache key from http request', () => {
        const params = new HttpParams().set('a', 'b');
        const httpRequest = new HttpRequest('GET', 'test', {params: params});
        const result = service.getCacheKey(httpRequest);
        expect(result).toEqual('testa=b');
    });
});
