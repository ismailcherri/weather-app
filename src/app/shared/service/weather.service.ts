import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {WeatherResponseDto} from 'src/app/shared/model/weather-response-dto';
import {environment} from 'src/environments/environment';
import {WeatherRequestByCity, WeatherRequestByCoordinates} from 'src/app/shared/model/weather-request';
import {Observable, of, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class WeatherService {
    constructor(private httpClient: HttpClient) {}

    public getWeatherByCity(request: WeatherRequestByCity): Observable<WeatherResponseDto> {
        return this.httpClient
            .get<WeatherResponseDto>(environment.apiUrl, {
                params: {appid: environment.apiKey, q: request.city || '', units: request.units},
            })
            .pipe(retry(3), catchError(this.handleError));
    }

    public getWeatherByCoordinates(request: WeatherRequestByCoordinates): Observable<WeatherResponseDto> {
        return this.httpClient
            .get<WeatherResponseDto>(environment.apiUrl, {
                params: {appid: environment.apiKey, lon: request.lon || 0, lat: request.lat || 0, units: request.units},
            })
            .pipe(retry(3), catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.status === 0) {
            console.error('An error occurred:', error.error);
        } else {
            console.error(`Backend returned code ${error.status}, body was: `, error.error);
        }
        return throwError(error.error.message);
    }
}
