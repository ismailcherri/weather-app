import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {WeatherService} from 'src/app/shared/service/weather.service';
import {HttpClient} from '@angular/common/http';
import {WeatherRequestByCity, WeatherRequestByCoordinates} from 'src/app/shared/model/weather-request';
import {WeatherResponseDto} from 'src/app/shared/model/weather-response-dto';
import {environment} from 'src/environments/environment';

describe('WeatherService', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    let service: WeatherService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
        });

        httpClient = TestBed.inject(HttpClient);
        httpTestingController = TestBed.inject(HttpTestingController);
        service = TestBed.inject(WeatherService);
    });

    afterEach(() => {
        // After every test, assert that there are no more pending requests.
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should get weather by city', () => {
        const request: WeatherRequestByCity = {
            city: 'city',
            units: 'standard',
        };

        const testData: WeatherResponseDto = {
            name: 'city',
        };

        service.getWeatherByCity(request).subscribe((response) => {
            expect(response).toEqual(testData);
        });

        const req = httpTestingController.expectOne(
            `${environment.apiUrl}?appid=${environment.apiKey}&q=${request.city}&units=${request.units}`,
        );
        expect(req.request.method).toEqual('GET');
        req.flush(testData);
    });

    it('should get weather by coordinates', () => {
        const request: WeatherRequestByCoordinates = {
            lon: 1,
            lat: 2,
            units: 'standard',
        };

        const testData: WeatherResponseDto = {
            coord: {
                lat: 1,
                lon: 2,
            },
        };

        service.getWeatherByCoordinates(request).subscribe((response) => {
            expect(response).toEqual(testData);
        });

        const req = httpTestingController.expectOne(
            `${environment.apiUrl}?appid=${environment.apiKey}&lon=${request.lon}&lat=${request.lat}&units=${request.units}`,
        );
        expect(req.request.method).toEqual('GET');
        req.flush(testData);
    });
});
