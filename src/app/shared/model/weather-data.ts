import {WeatherResponseDto} from 'src/app/shared/model/weather-response-dto';

export class WeatherData {
    public longitude?: number;
    public latitude?: number;
    public cityName?: string;
    public temperature?: number;
    public feelsLike?: number;
    public maxTemperature?: number;
    public minTemperature?: number;
    public humidity?: number;
    public pressure?: number;
    public weatherDescription?: string;
    public cloudiness?: number;
    public windSpeed?: number;
    public windDirection?: number;
    public rainVolumeLastHour?: number;
    public snowVolumeLastHour?: number;

    constructor(dto: WeatherResponseDto) {
        this.longitude = dto.coord?.lon;
        this.latitude = dto.coord?.lat;
        this.cityName = dto.name;
        this.temperature = dto.main?.temp;
        this.feelsLike = dto.main?.feels_like;
        this.maxTemperature = dto.main?.temp_max;
        this.minTemperature = dto.main?.temp_min;
        this.humidity = dto.main?.humidity;
        this.pressure = dto.main?.pressure;
        this.weatherDescription = dto.weather && dto.weather.length > 0 ? dto.weather[0].description : undefined;
        this.cloudiness = dto.clouds?.all;
        this.windSpeed = dto.wind?.speed;
        this.windDirection = dto.wind?.deg;
        this.rainVolumeLastHour = dto.rain?.['1h'];
        this.snowVolumeLastHour = dto.snow?.['1h'];
    }
}
