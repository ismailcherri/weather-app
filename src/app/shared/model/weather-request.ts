export type UnitsType = 'standard' | 'metric' | 'imperial';

export interface WeatherRequest {
    units: UnitsType;
}

export interface WeatherRequestByCity extends WeatherRequest {
    city: string;
}

export interface WeatherRequestByCoordinates extends WeatherRequest {
    lon: number;
    lat: number;
}
