export interface Coord {
    lon: number;
    lat: number;
}

export interface Main {
    temp: number;
    feels_like: number;
    temp_min: number;
    temp_max: number;
    pressure: number;
    humidity: number;
}

export interface Weather {
    main: string;
    description: string;
}

export interface Wind {
    speed: number;
    deg: number;
    gust: number;
}

export interface Clouds {
    all: number;
}

export interface Rain {
    '1h': number;
    '3h': number;
}

export interface Snow {
    '1h': number;
    '3h': number;
}

export interface WeatherResponseDto {
    coord?: Coord;
    main?: Main;
    weather?: Weather[];
    visibility?: number;
    wind?: Wind;
    clouds?: Clouds;
    rain?: Rain;
    snow?: Snow;
    dt?: number;
    timezone?: number;
    name?: string;
}
