import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {CacheInterceptor} from 'src/app/shared/interceptor/cache-interceptor';
import {WeatherService} from 'src/app/shared/service/weather.service';
import {WeatherRequestByCity} from 'src/app/shared/model/weather-request';
import {WeatherResponseDto} from 'src/app/shared/model/weather-response-dto';
import {environment} from 'src/environments/environment';

describe('CacheInterceptor', () => {
    let httpTestingController: HttpTestingController;
    let service: WeatherService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: CacheInterceptor,
                    multi: true,
                },
                WeatherService,
            ],
        });

        httpTestingController = TestBed.inject(HttpTestingController);
        service = TestBed.inject(WeatherService);
    });

    afterEach(() => {
        // After every test, assert that there are no more pending requests.
        httpTestingController.verify();
    });
    it('should intercept GET requests', () => {
        const request: WeatherRequestByCity = {
            city: 'city',
            units: 'standard',
        };

        const testData: WeatherResponseDto = {
            name: 'city',
        };

        service.getWeatherByCity(request).subscribe((response) => {
            expect(response).toEqual(testData);
        });

        service.getWeatherByCity(request);
        const req = httpTestingController.expectOne(
            `${environment.apiUrl}?appid=${environment.apiKey}&q=${request.city}&units=${request.units}`,
        );
        expect(req.request.method).toEqual('GET');
        req.flush(testData);
    });
});
