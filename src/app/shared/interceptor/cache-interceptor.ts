import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {tap} from 'rxjs/operators';
import {RequestCacheService} from 'src/app/shared/service/request-cache.service';

@Injectable()
export class CacheInterceptor implements HttpInterceptor {
    private ttl: number = 1000 * 60 * 5; // 5 minutes
    private current: number;

    constructor(private cache: RequestCacheService) {
        this.current = Date.now();
    }

    intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        if (!this.shouldCache(httpRequest)) {
            return next.handle(httpRequest);
        }

        if (this.isCacheExpired()) {
            this.current = Date.now();
            return next.handle(httpRequest);
        }

        const cachedResponse = this.cache.get(httpRequest);
        if (cachedResponse) {
            return of(cachedResponse);
        }
        return this.sendRequest(httpRequest, next, this.cache);
    }

    private shouldCache(req: HttpRequest<unknown>): boolean {
        return req.method === 'GET';
    }

    private isCacheExpired(): boolean {
        return Date.now() - this.current > this.ttl;
    }

    private sendRequest(req: HttpRequest<any>, next: HttpHandler, cache: RequestCacheService): Observable<HttpEvent<unknown>> {
        return next.handle(req).pipe(
            tap((event) => {
                if (event instanceof HttpResponse) {
                    cache.put(req, event);
                }
            }),
        );
    }
}
