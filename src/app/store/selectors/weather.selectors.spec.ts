import * as fromWeather from 'src/app/store/reducers/weather.reducer';
import {WeatherState} from 'src/app/store/reducers/weather.reducer';
import {
    selectWeatherState,
    selectWeatherStateData,
    selectWeatherStateError,
    selectWeatherStatePending,
} from 'src/app/store/selectors/weather.selectors';
import {WeatherData} from 'src/app/shared/model/weather-data';

describe('Weather Selectors', () => {
    const state: WeatherState = {
        pending: true,
        error: 'some error',
        data: new WeatherData({
            name: 'city',
            coord: {
                lon: 1,
                lat: 2,
            },
        }),
    };

    it('should select the feature state', () => {
        const result = selectWeatherState({
            [fromWeather.weatherFeatureKey]: {pending: false},
        });

        expect(result).toEqual({pending: false});
    });

    it('should select weather data', () => {
        const result = selectWeatherStateData.projector(state);
        expect(result).toEqual(state.data);
    });

    it('should select pending', () => {
        const result = selectWeatherStatePending.projector(state);
        expect(result).toEqual(state.pending);
    });

    it('should select error', () => {
        const result = selectWeatherStateError.projector(state);
        expect(result).toEqual(state.error);
    });
});
