import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromWeather from 'src/app/store/reducers/weather.reducer';

export const selectWeatherState = createFeatureSelector<fromWeather.WeatherState>(fromWeather.weatherFeatureKey);

export const selectWeatherStateData = createSelector(selectWeatherState, (state) => state && state.data);

export const selectWeatherStatePending = createSelector(selectWeatherState, (state) => state && state.pending);

export const selectWeatherStateError = createSelector(selectWeatherState, (state) => state && state.error);
