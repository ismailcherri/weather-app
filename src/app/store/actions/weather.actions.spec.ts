import * as fromWeather from 'src/app/store/actions/weather.actions';
import {WeatherRequest, WeatherRequestByCity, WeatherRequestByCoordinates} from 'src/app/shared/model/weather-request';

describe('loadWeathers', () => {
    it('should return an action', () => {
        const requestByCity: WeatherRequestByCity = {
            city: 'city',
            units: 'standard',
        };

        expect(fromWeather.loadWeathersByCity({request: requestByCity}).type).toBe('[Weather] Load Weathers By City');
        const requestByCoordinates: WeatherRequestByCoordinates = {
            lon: 1,
            lat: 1,
            units: 'standard',
        };
        expect(fromWeather.loadWeathersByCoordinates({request: requestByCoordinates}).type).toBe('[Weather] Load Weathers By Coordinates');
    });
});
