import {createAction, props} from '@ngrx/store';
import {WeatherRequestByCity, WeatherRequestByCoordinates} from 'src/app/shared/model/weather-request';
import {WeatherData} from 'src/app/shared/model/weather-data';

export const loadWeathersByCity = createAction('[Weather] Load Weathers By City', props<{request: WeatherRequestByCity}>());
export const loadWeathersByCoordinates = createAction(
    '[Weather] Load Weathers By Coordinates',
    props<{request: WeatherRequestByCoordinates}>(),
);

export const loadWeathersSuccess = createAction('[Weather] Load Weathers Success', props<{data: WeatherData}>());

export const loadWeathersFailure = createAction('[Weather] Load Weathers Failure', props<{error: any}>());
