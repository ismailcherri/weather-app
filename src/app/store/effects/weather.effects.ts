import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, concatMap, map} from 'rxjs/operators';
import {of} from 'rxjs';

import * as WeatherActions from 'src/app/store/actions/weather.actions';
import {WeatherService} from 'src/app/shared/service/weather.service';
import {WeatherData} from 'src/app/shared/model/weather-data';

@Injectable()
export class WeatherEffects {
    loadWeathersByCity$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(WeatherActions.loadWeathersByCity),
            concatMap((action) =>
                this.weatherService.getWeatherByCity(action.request).pipe(
                    map((data) => WeatherActions.loadWeathersSuccess({data: new WeatherData(data)})),
                    catchError((error) => of(WeatherActions.loadWeathersFailure({error}))),
                ),
            ),
        );
    });

    loadWeathersByCoordinates$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(WeatherActions.loadWeathersByCoordinates),
            concatMap((action) =>
                this.weatherService.getWeatherByCoordinates(action.request).pipe(
                    map((data) => WeatherActions.loadWeathersSuccess({data: new WeatherData(data)})),
                    catchError((error) => of(WeatherActions.loadWeathersFailure({error}))),
                ),
            ),
        );
    });

    constructor(private actions$: Actions, private weatherService: WeatherService) {}
}
