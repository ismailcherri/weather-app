import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {Observable, of} from 'rxjs';

import {WeatherEffects} from 'src/app/store/effects/weather.effects';
import {SharedModule} from 'src/app/shared/shared.module';
import {WeatherService} from 'src/app/shared/service/weather.service';
import {loadWeathersByCity, loadWeathersByCoordinates, loadWeathersSuccess} from 'src/app/store/actions/weather.actions';
import {WeatherResponseDto} from 'src/app/shared/model/weather-response-dto';
import {WeatherData} from 'src/app/shared/model/weather-data';

describe('WeatherEffects', () => {
    let actions$: Observable<any>;
    let effects: WeatherEffects;
    let weatherService: WeatherService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [SharedModule],
            providers: [WeatherEffects, provideMockActions(() => actions$)],
        });

        effects = TestBed.inject(WeatherEffects);
        weatherService = TestBed.inject(WeatherService);
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadWeathersByCity$', () => {
        it('should load weather by city', (done) => {
            const expectedRequest: WeatherResponseDto = {name: 'city'};
            const expectedData: WeatherData = new WeatherData({name: 'city'});
            const spy = spyOn(weatherService, 'getWeatherByCity').and.returnValue(of(expectedRequest));
            actions$ = of(loadWeathersByCity);
            effects.loadWeathersByCity$.subscribe((res) => {
                expect(res).toEqual(loadWeathersSuccess({data: expectedData}));
                expect(spy).toHaveBeenCalledTimes(1);
                done();
            });
        });
    });

    describe('loadWeathersByCoordinates$', () => {
        it('should load weather by city', (done) => {
            const expectedRequest: WeatherResponseDto = {coord: {lon: 1, lat: 1}};
            const expectedData: WeatherData = new WeatherData({coord: {lon: 1, lat: 1}});
            const spy = spyOn(weatherService, 'getWeatherByCoordinates').and.returnValue(of(expectedRequest));
            actions$ = of(loadWeathersByCoordinates);
            effects.loadWeathersByCoordinates$.subscribe((res) => {
                expect(res).toEqual(loadWeathersSuccess({data: expectedData}));
                expect(spy).toHaveBeenCalledTimes(1);
                done();
            });
        });
    });
});
