import {reducer, initialState, WeatherState} from 'src/app/store/reducers/weather.reducer';
import {
    loadWeathersByCity,
    loadWeathersByCoordinates,
    loadWeathersFailure,
    loadWeathersSuccess,
} from 'src/app/store/actions/weather.actions';
import {WeatherData} from 'src/app/shared/model/weather-data';

describe('Weather Reducer', () => {
    describe('an unknown action', () => {
        it('should return the previous state', () => {
            const action = {} as any;

            const result = reducer(initialState, action);

            expect(result).toBe(initialState);
        });

        it('should set pending to true', () => {
            const action = loadWeathersByCity({request: {city: 'city', units: 'standard'}});
            const newState: WeatherState = {...initialState, pending: true};

            const result = reducer(initialState, action);
            expect(result).toEqual(newState);
            expect(result).not.toBe(newState);
        });

        it('should set pending to true', () => {
            const action = loadWeathersByCoordinates({request: {lat: 1, lon: 1, units: 'standard'}});
            const newState: WeatherState = {...initialState, pending: true};

            const result = reducer(initialState, action);
            expect(result).toEqual(newState);
            expect(result).not.toBe(newState);
        });

        it('should set data and pending to false', () => {
            const action = loadWeathersSuccess({data: new WeatherData({name: 'city'})});
            const newState: WeatherState = {...initialState, data: action.data, pending: false};

            const result = reducer(initialState, action);
            expect(result).toEqual(newState);
            expect(result).not.toBe(newState);
        });

        it('should set error and pending to false', () => {
            const action = loadWeathersFailure({error: 'some error'});
            const newState: WeatherState = {...initialState, error: 'some error', pending: false};

            const result = reducer(initialState, action);
            expect(result).toEqual(newState);
            expect(result).not.toBe(newState);
        });
    });
});
