import {createReducer, on} from '@ngrx/store';
import * as WeatherActions from 'src/app/store/actions/weather.actions';
import {WeatherData} from 'src/app/shared/model/weather-data';

export const weatherFeatureKey = 'weather';

export interface WeatherState {
    data?: WeatherData;
    error?: string;
    pending: boolean;
}

export const initialState: WeatherState = {
    data: undefined,
    error: undefined,
    pending: false,
};

export const reducer = createReducer(
    initialState,
    on(WeatherActions.loadWeathersByCity, (state) => ({...state, pending: true})),
    on(WeatherActions.loadWeathersByCoordinates, (state) => ({...state, pending: true, error: undefined})),
    on(WeatherActions.loadWeathersSuccess, (state, action) => ({...state, data: action.data, pending: false, error: undefined})),
    on(WeatherActions.loadWeathersFailure, (state, action) => ({...state, error: action.error, pending: false})),
);
