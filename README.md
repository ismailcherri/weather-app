# AngularWeatherapp

A demo Angular app to display weather data from Open Weather Map API

## Requirement

1. Node v12+
2. NPM v6+
3. Open Weather Map API key (provided)

## Setup

1. Clone this repository
2. run `npm install`

## Running the app

Run `npm start` for a dev server.  
Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Testing

Run `npm run test`
or
Run `npm run test-headless`

## Additional information

The app is built using Angular 12 and uses NgRx for state management. Bootstrap 4 was used to
give basic styling.
Basic caching is done through http interceptor.

Things I would like to improve:

1. Refactor the store to allow for additional sub-states
2. Replace Karma with Jest
3. Add E2E tests via Cypress
4. Adds the ability to switch between different units e.g. metric and imperial
